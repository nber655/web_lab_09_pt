"use strict";

// Reset animation delays on page load
window.onload = function() {
    resetanimationDelay();
}

// Animation delay reset and event listener function
function resetanimationDelay() {
    var bookImages = document.getElementsByClassName("page");
        for (var property in bookImages) {
            bookImages[property].style.animationDelay = "0s";
            bookImages[property].addEventListener("click", bookAnimation);
        }
}

// Book animation function
var y=0;
var x=0;
function bookAnimation() {
    
    var imageFlipping = document.getElementsByClassName("page");
    // Add animation to element
    imageFlipping[y].classList.add("pageAnimation");
    
   // Bring next image up front in the stacking
   // I refer to the solutions to figure this out !!!
    x=y+1;
    imageFlipping[y].addEventListener("animationend", function() {
            imageFlipping[x].style.zIndex = 1;
            });
    
    y++;

    }


