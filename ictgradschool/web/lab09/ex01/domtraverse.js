"use strict";

/* Your answer here */

// Change element style by class name
var ex01_1_element = document.getElementsByClassName("text1"); // This will return a array because there might be several elements with the same class
ex01_1_element[0].style.backgroundColor = "pink";

// Text of last paragraph changed to red
var ex01_2_element = document.getElementById("footer"); // This return an element because the element id must be unique
ex01_2_element.style.color = "red";

// Hide the second div element
"text2"
// var ex01_3a_element = document.getElementsByClassName("text2"); 
// ex01_3a_element[0].style.display = "none"; // This  hides the entire element div with class "text2"

var ex01_3b_element = document.getElementsByClassName("text2");
ex01_3b_element[0].style.visibility = "hidden" // This  hides only the content of the element div with class "text2"

// Change the content of the first paragraph element
var ex01_4_element = document.getElementsByClassName("subtitle");
ex01_4_element[0].innerHTML = "\"Hello World\"";

//Make all the paragraph bold when clicking on buttom
function change_all_par_style() {
    var paragraph = document.getElementsByTagName("p");
    for(var i = 0; i < paragraph.length; i++ ) {
        var inner_element = paragraph[i];
        inner_element.style.fontWeight = "bold";
    }
}


window.onload = function(event) {     // This should be always added when using event handler   
      // The event handler is necesary to make the button call the function when clicked
      var button = document.getElementsByTagName("button")[0];
      button.onclick = change_all_par_style;
}

// Function to change style for all HTML elements with a specified class name
// var full_html_elements = document.getElementsByClassName("text1");
// function changeclass() {
//     var full_html_elements = document.getElementsByClassName("text1")
//     //Returned array and index to be changed
//     for(var i = 0; i < full_html_elements.length; i++) {
        
//         //Split the array by elements to be able to change the style
//         var inner_element = full_html_elements[i];
//         inner_element.style.backgroundcolor = ;
       
//     }

// }
