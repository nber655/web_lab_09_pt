"use strict";

window.onload = function() {

    var pages = document.getElementsByClassName("page");

    for (var i = 0; i < pages.length; i++) {

        // Disable animation-delay on all pages
        pages[i].style.animationDelay = "initial";

        // When you click a page, animate it
        pages[i].onclick = animatePage;
    }

}

function animatePage() {

    // Animate the page by setting the class to the animated class.
    // "this" will refer to "this particular page that's been clicked".
    this.classList.add("pageAnimation");

    // Get the next page which will be animated. See:
    // https://www.w3schools.com/jsref/prop_element_nextelementsibling.asp
    var nextPage = this.nextElementSibling;
    if (nextPage) {
        // When the animation is finished, bring the next page to the front z-index so it can be clicked.
        this.addEventListener("animationend", function() {
            nextPage.style.zIndex = 1;
        });

    }
}