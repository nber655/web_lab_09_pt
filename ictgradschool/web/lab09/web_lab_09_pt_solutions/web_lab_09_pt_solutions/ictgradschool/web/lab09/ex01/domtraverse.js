"use strict";

/* Your answer here */

// Make the first div pink
var divs = document.getElementsByTagName("div");
divs[0].style.backgroundColor = "pink";

// Make the last paragraph have red text
var ps = document.getElementsByTagName("p");
ps[ps.length - 1].style.color = "red";

// Hide the second div
divs[1].style.opacity = 0;

// Change the text in the first paragraph to "hello world"
ps[0].innerHTML = "Hello World";

// When you click on the button, make all paragraphs bold
document.getElementById("makeBold").onclick = function() {
    for (var i = 0; i < ps.length; i++) {
        ps[i].style.fontWeight = "bold";
    }
}