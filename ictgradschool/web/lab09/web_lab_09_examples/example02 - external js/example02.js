"use strict";

// This file contains the same JavaScript as the internal <script> tag in the <head> element of exmaple01.html.
// This time it's defined in its own file.

alert("I'm in an external JS file!");

console.log("Logging!");

function sayHi(name) {
    alert("Hi, " + name + "!");
}

function alertPageLoaded() {
    alert("The page has loaded!");
}