"use strict";

window.onload = function() {

    var divs = document.getElementsByClassName("mydiv");

    // Doesn't work because "i" in the function will actually be a MouseEvent rather than an int!
    /*for(var i = 0; i < divs.length; i++) {
        divs[i].onmouseover = function(i) {
        console.log(i);
        divs[i].style.color = "red";
    }
    }*/

    // Also doesn't work because "div" will be set to the last element in the array by the time the
    // mouseover function is called.
    /*for(var i = 0; i < divs.length; i++) {
        var div = divs[i];
        div.onmouseover = function(event) {
        div.style.color = "red";
    }
    }*/

    // here we're defining a function that takes i and color as args. The values are set as soon as the function is called.
    function divMouseOver_Closure(i, color) {

        // This function returns the actual function that will be called when the event is handled, but doing it this way we
        // will have access to the exact value of i and color we want.
        return function(event) {
            console.log(i);
            console.log(event);
            divs[i].style.color = color;
        }
    }

    // For each div, the value of i  and color will be set when we call the divMouseOver_Closure function.
    for(var i = 0; i < divs.length; i++) {
        divs[i].onmouseover = divMouseOver_Closure(i, "red");
        divs[i].onmouseout = divMouseOver_Closure(i, "black");
    }

}